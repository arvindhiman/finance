package domain;

import java.math.BigDecimal;
import java.util.Date;

/**
 */
public class ZeroCouponBond {

    private static final int PERIODICITY = 2;

    private BigDecimal faceValue;
    private Date startDate;
    private Date maturityDate;


    public BigDecimal getFaceValue() {
        return faceValue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

}
