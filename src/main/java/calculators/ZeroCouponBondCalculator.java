package calculators;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by arvinddhiman on 9/2/17.
 */
public class ZeroCouponBondCalculator {

    BigDecimal TWO = new BigDecimal("2.0");

    public double calculateAPR(BigDecimal faceValue, BigDecimal price, int yearsToMaturity) {

        BigDecimal factor = faceValue.divide(price, 5, RoundingMode.HALF_UP);

        double totalPeriods = yearsToMaturity * 2.0;

        double logOfFactor = Math.log(factor.doubleValue());

        BigDecimal power = new BigDecimal(logOfFactor/totalPeriods);

        double exponentValue = Math.exp(power.doubleValue());

        return 2 * (exponentValue - 1);

    }

    public BigDecimal calculateContinuousCompoudingAPR(BigDecimal apr, int periodicity) {

        double calculatedAPR = periodicity * (Math.log(1 + (apr.divide(TWO).doubleValue())));
        return new BigDecimal(calculatedAPR).setScale( 5, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal convertPeriodicityOfAPR(BigDecimal apr, int aprPeriodicity, int newPeriodicity) {

        BigDecimal continuousCompoundingAPR = calculateContinuousCompoudingAPR(apr, aprPeriodicity);
        double calculatedNewAPR = newPeriodicity * (Math.exp(continuousCompoundingAPR.doubleValue()/newPeriodicity) - 1);
        return new BigDecimal(calculatedNewAPR).setScale( 5, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal calculatePrice(BigDecimal faceValue, BigDecimal apr, int yearsToMaturity) {


        BigDecimal power = TWO.multiply(new BigDecimal(yearsToMaturity));

        double denominator2 = apr.divide(TWO,5, BigDecimal.ROUND_HALF_UP).doubleValue();
        denominator2 = 1 + denominator2;

        double denominator = Math.pow(denominator2, power.doubleValue());

        return faceValue.divide(new BigDecimal(denominator).setScale(5, BigDecimal.ROUND_HALF_UP), 5, BigDecimal.ROUND_HALF_UP);
    }
}
