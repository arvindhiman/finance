import calculators.ZeroCouponBondCalculator;

import java.math.BigDecimal;

/**
 * Created by arvinddhiman on 9/2/17.
 */
public class Util {

    public static void main(String[] args) {

        BigDecimal faceValue = new BigDecimal("1000");
        BigDecimal price = new BigDecimal("50");
        int yearsToMaturity = 30;


        System.out.println(new ZeroCouponBondCalculator().calculateAPR(faceValue, price, yearsToMaturity));

    }
}
