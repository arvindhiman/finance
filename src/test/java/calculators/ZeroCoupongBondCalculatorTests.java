package calculators;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by arvinddhiman on 9/2/17.
 */

public class ZeroCoupongBondCalculatorTests {

    ZeroCouponBondCalculator zeroCouponBondCalculator = new ZeroCouponBondCalculator();

    @Test
    public void test1_BondAPRIsCalculatedRight() {
        BigDecimal faceValue = new BigDecimal("1000");
        BigDecimal price = new BigDecimal("50");
        int yearsToMaturity = 30;

        BigDecimal calculatedApr = new BigDecimal(zeroCouponBondCalculator.calculateAPR(faceValue, price, yearsToMaturity));

        BigDecimal expectedAPR = new BigDecimal("0.10239");

        BigDecimal apr = calculatedApr.setScale(5, BigDecimal.ROUND_HALF_UP);

        System.out.println(apr);

        Assert.assertEquals(apr, expectedAPR);

    }

    @Test
    public void test2_BondAPRIsCalculatedRight() {
        BigDecimal faceValue = new BigDecimal("1000");
        BigDecimal price = new BigDecimal("250");
        int yearsToMaturity = 14;

        BigDecimal calculatedApr = new BigDecimal(zeroCouponBondCalculator.calculateAPR(faceValue, price, yearsToMaturity));

        BigDecimal expectedAPR = new BigDecimal("0.10151");

        BigDecimal apr = calculatedApr.setScale(5, BigDecimal.ROUND_HALF_UP);

        System.out.println(apr);

        Assert.assertEquals(apr, expectedAPR);

    }

    @Test
    public void test3_BondAPRIsCalculatedRight() {
        BigDecimal faceValue = new BigDecimal("100");
        BigDecimal price = new BigDecimal("60");
        int yearsToMaturity = 10;

        BigDecimal calculatedApr = new BigDecimal(zeroCouponBondCalculator.calculateAPR(faceValue, price, yearsToMaturity));

        BigDecimal expectedAPR = new BigDecimal("0.05174");

        BigDecimal apr = calculatedApr.setScale(5, BigDecimal.ROUND_HALF_UP);

        System.out.println(apr);

        Assert.assertEquals(apr, expectedAPR);

    }

    @Test
    public void testCalculateContinuousCompoundingAPR() {
        int periodicity = 2;
        BigDecimal apr = new BigDecimal("0.05174");

        BigDecimal continousAPR = zeroCouponBondCalculator.calculateContinuousCompoudingAPR(apr, periodicity);

        BigDecimal expectedAPR = new BigDecimal("0.05108");

        System.out.println(continousAPR);

        Assert.assertEquals(continousAPR, expectedAPR);

        System.out.println(zeroCouponBondCalculator.convertPeriodicityOfAPR(apr, 2, 4));

    }

    @Test
    public void testCalculatePrice1() {

        BigDecimal apr = new BigDecimal("0.05174");

        BigDecimal faceValue = BigDecimal.TEN.multiply(BigDecimal.TEN);

        for (int i = 10; i >= 0; i--) {
            System.out.println("YearToMaturity: " + i + " Price: " + zeroCouponBondCalculator.calculatePrice(faceValue, apr, i));
        }

    }
}
